/*
 * Main.cpp
 *
 *  Created on: Mar 20, 2020
 *      Author: bill
 */

#include <Sys/Include/SysDef.hpp>
#include <Eden/Include/Gcd/Gcd.hpp>
#include <Eden/Include/Core/CoreMem.hpp>
#include <cstring>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <iostream>
#include <mutex>
#include <map>

const _UINT NUM_SENDS = 1000*60;
const _UINT NUM_PORTS=SYS::UDP_Port_RANGE_END - SYS::UDP_Port_RANGE_BEGIN + 1;
const _UINT RETRY_COUNT = 60;
const chrono::milliseconds TIMER_PERIOD_SEND(97);
const chrono::milliseconds TIMER_PERIOD_RETRANSMIT(499);

int SocketFd_;
sockaddr_in ServerAdr_;
_UINT SendCount_ = 0;
mutex Mutex_;
map<_UUID,_UINT> mapSent_;



void _SendPacket(_UUID uuid) {
	_UINT16 PortNo = SYS::UDP_Port_RANGE_BEGIN + rand() % NUM_PORTS;
	ServerAdr_.sin_port = htons(PortNo);

	SYS::MASK_REQUEST Request;
	Request.Id_ = uuid;
	Request.Birthday.Month_=0;
	Request.Birthday.Day_=0;
	Request.DistributorId_=0;
	_OBJ<IMem> Mem=Request.Serialize();

	sendto(SocketFd_,Mem->Ptr(),Mem->DataLen(),0,(struct sockaddr *) &ServerAdr_,sizeof(ServerAdr_));
}



// Period: 499 milliseconds, retransmit all non-acknowledged requests
bool TimerProc_Retransmit() {
	Mutex_.lock();
	_UINT vectSentSize = mapSent_.size();
	if ( vectSentSize ) {
		cout << "Retransmit non-acknowledged requests: " <<  vectSentSize << endl;
		for (auto &Iter : mapSent_) {
			_SendPacket(Iter.first);
			--Iter.second;
		}
	}
	auto Iter = mapSent_.begin();
	while (Iter != mapSent_.end()) {
		if ( Iter->second == 0 ) {
			cout << "Failed: " << _STRING(Iter->first) << endl;
			Iter = mapSent_.erase(Iter);
		}
		else {
			++Iter;
		}
	}
	Mutex_.unlock();
	return true;
}



// Period: 97 milliseconds, send 100 requests => 1000 requests per second.
bool TimerProc_Send() {

	if ( SendCount_ < NUM_SENDS) {
		_UUID Uuid;
		Mutex_.lock();
		for (_UINT i=0; i<100; i++) {
			Uuid.Generate();
			_SendPacket(Uuid);
			mapSent_.insert( pair<_UUID,_UINT>(Uuid,RETRY_COUNT) );
		}
		SendCount_ += 100;
		Mutex_.unlock();
	}

	_UINT numNonAck=0;
	Mutex_.lock();
	numNonAck = mapSent_.size();
	Mutex_.unlock();

	if ( SendCount_ < NUM_SENDS || numNonAck != 0) {
		return true;
	}
	else {
		kill(getpid(),SIGKILL);
		GCD::Shutdown();
		return false;
	}
}



void ProcessAck(_OBJ<IMem> raw_packet) {
	SYS::MASK_REQUEST Request;
	if ( SYS::Deserialize(&Request,raw_packet) ) {
		_UUID Uuid_ = Request.Id_;
		Mutex_.lock();
		auto Iter = mapSent_.find(Uuid_);
		if ( Iter != mapSent_.end() ) {
			mapSent_.erase(Iter);
		}
		Mutex_.unlock();
	}
}



void AckReceiver() {
	_BYTE *ptrPacketBuf = new _BYTE[65536];
	struct sockaddr_in ServerAdr;
	socklen_t AdrSize;
	while (true) {
		AdrSize = sizeof(ServerAdr);
		ssize_t PacketLen = recvfrom(SocketFd_,ptrPacketBuf,65536,0,( struct sockaddr *)&ServerAdr,&AdrSize);
		_OBJ<IMem> PacketData = _MEM::New(PacketLen,ptrPacketBuf);
		ProcessAck(PacketData);
	}
}



void Run() {
	SYS::Initialize();
	SocketFd_ = ::socket(AF_INET, SOCK_DGRAM, 0);
	_XASSERT(SocketFd_ >= 0);
	GCD::Dispatch(TIMER_PERIOD_SEND,TimerProc_Send);
	GCD::Dispatch(TIMER_PERIOD_RETRANSMIT,TimerProc_Retransmit);
	GCD::Dispatch(AckReceiver);
}



void Shutdown() {
	SYS::Shutdown();
}



void UnixSignalHandler_INT(int signal) {
	kill(getpid(),SIGKILL);
	GCD::Shutdown();
}



bool GetServerIpAdr(char *host_name) {
	hostent *ptrHostInfo = gethostbyname(host_name);
	if ( ptrHostInfo == NULL ) {
		return false;
	}
	memset((_BYTE *)&ServerAdr_, 0, sizeof(ServerAdr_));
	ServerAdr_.sin_family = AF_INET;
	ServerAdr_.sin_port = htons(0);
	in_addr **pPtr_AdrList = (struct in_addr **)ptrHostInfo->h_addr_list;
	ServerAdr_.sin_addr = *pPtr_AdrList[0];
	return true;
}



int main(int argc,char *argv[]) {
	if ( argc != 2 || !GetServerIpAdr(argv[1]) ) {
		cout << "Usage: UdpSend [ServerAddress]" << endl;
		return -1;
	}
	::signal(SIGINT,UnixSignalHandler_INT);
	GCD::Run([]{Run();},[]{Shutdown();});
	return 0;
}
