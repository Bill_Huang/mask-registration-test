/*
 * Main.cpp
 *
 *  Created on: Mar 20, 2020
 *      Author: bill
 */

#include <Sys/Include/SysDef.hpp>
#include <Eden/Include/Gcd/Gcd.hpp>
#include <Eden/Include/Core/CoreMem.hpp>
#include <cstring>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <iostream>
#include <mutex>
#include <set>

const _UINT NUM_SENDS = 1000*60;
const _UINT numAvailablePorts_=SYS::UDP_Port_RANGE_END - SYS::UDP_Port_RANGE_BEGIN + 1;
const chrono::milliseconds TIMER_PERIOD_SEND(100);
const chrono::milliseconds TIMER_PERIOD_RESEND(1000);

int SocketFd_;
GCD::TASK_QUEUE_ID NetTaskQueue_;
sockaddr_in ServerAdr_;
_UINT SendCount_ = 0;
mutex Mutex_;
set<_UUID> vectSent_;



void _SendPacket(_UUID uuid) {
	_UINT16 PortNo = SYS::UDP_Port_RANGE_BEGIN + rand() % numAvailablePorts_;
	ServerAdr_.sin_port = htons(PortNo);

	SYS::MASK_REQUEST Request;
	Request.Id_ = uuid;
	Request.Birthday.Month_=0;
	Request.Birthday.Day_=0;
	Request.DistributorId_=0;
	_OBJ<IMem> Mem=Request.Serialize();

	sendto(SocketFd_,Mem->Ptr(),Mem->DataLen(),0,(struct sockaddr *) &ServerAdr_,sizeof(ServerAdr_));
	++SendCount_;
}



// Period: 1000 milliseconds, retransmit all non-acknowledged requests
bool TimerProc_Retransmit() {
	Mutex_.lock();
	_UINT vectSentSize_ = vectSent_.size();
	cout << "Retransmit non-acknowledged requests: " <<  vectSentSize_ << endl;
	for (auto &Iter : vectSent_) {
		_SendPacket(Iter);
	}
	vectSent_.clear();
	Mutex_.unlock();
	return true;
}



// Period: 100 milliseconds, send 100 requests => 1000 requests per second.
bool TimerProc_Send() {

	_UUID Uuid;
	Mutex_.lock();
	for (_UINT i=0; i<100; i++) {
		Uuid.Generate();
		_SendPacket(Uuid);
		vectSent_.insert(Uuid);
	}
	Mutex_.unlock();

	if ( SendCount_ < NUM_SENDS) {
		return true;
	}
	else {
		GCD::Shutdown();
		return false;
	}
}



void ProcessAck(_OBJ<IMem> raw_packet) {
	_UUID Uuid_;

	SYS::MASK_REQUEST Request;
	if ( SYS::Deserialize(&Request,raw_packet) ) {
		_UUID Uuid_ = Request.Id_;
		Mutex_.lock();
		auto Iter = vectSent_.find(Uuid_);
		if ( Iter != vectSent_.end() ) {
			vectSent_.erase(Iter);
		}
		Mutex_.unload();
	}
}



void AckReceiver(_UINT16 port_no) {
	int SocketFd = ::socket(AF_INET, SOCK_DGRAM, 0);
	_XASSERT(SocketFd >= 0);

	int optval = 1;
	::setsockopt(SocketFd, SOL_SOCKET, SO_REUSEADDR,(const void *)&optval, sizeof(int));

	struct sockaddr_in ServerAdr;
	memset((_BYTE *)&ServerAdr, 0, sizeof(ServerAdr));
	ServerAdr.sin_family = AF_INET;
	ServerAdr.sin_addr.s_addr = htonl(INADDR_ANY);
	ServerAdr.sin_port = htons(port_no);

	int retBind=bind(SocketFd, (struct sockaddr *)&ServerAdr,sizeof(ServerAdr));
	_XASSERT(retBind >= 0);

	_BYTE *ptrPacketBuf = new _BYTE[65536];
	struct sockaddr_in ClientAdr;
	socklen_t AdrSize;
	while (true) {
		AdrSize = sizeof(ClientAdr);
		ssize_t PacketLen = recvfrom(SocketFd,ptrPacketBuf,65536,0,( struct sockaddr *)&ClientAdr,&AdrSize);
		_OBJ<IMem> PacketData = _MEM::New(PacketLen,ptrPacketBuf);
		ProcessAck(PacketData);
	}
}



void Run() {
	SocketFd_ = ::socket(AF_INET, SOCK_DGRAM, 0);
	_XASSERT(SocketFd_ >= 0);
	NetTaskQueue_ = GCD::CreateQueue(numAvailablePorts_);
	GCD::Dispatch(TIMER_PERIOD_SEND,TimerProc_Send);
	GCD::Dispatch(TIMER_PERIOD_RESEND,TimerProc_Retransmit);
	for (_UINT16 Port=SYS::UDP_Port_RANGE_BEGIN; Port <= SYS::UDP_Port_RANGE_END; Port++) {
		GCD::Dispatch([=]{
			AckReceiver(Port);
		},NetTaskQueue_);
	}
}



void Shutdown() {
	// NULL
}



void UnixSignalHandler_INT(int signal) {
	GCD::Shutdown();
}



bool GetServerIpAdr(char *host_name) {
	hostent *ptrHostInfo = gethostbyname(host_name);
	if ( ptrHostInfo == NULL ) {
		return false;
	}
	memset((_BYTE *)&ServerAdr_, 0, sizeof(ServerAdr_));
	ServerAdr_.sin_family = AF_INET;
	ServerAdr_.sin_port = htons(0);
	in_addr **pPtr_AdrList = (struct in_addr **)ptrHostInfo->h_addr_list;
	ServerAdr_.sin_addr = *pPtr_AdrList[0];
	return true;
}



int main(int argc,char *argv[]) {
	if ( argc != 2 || !GetServerIpAdr(argv[1]) ) {
		cout << "Usage: UdpSend [ServerAddress]" << endl;
		return -1;
	}
	::signal(SIGINT,UnixSignalHandler_INT);
	GCD::Run([]{Run();},[]{Shutdown();});
	return 0;
}
