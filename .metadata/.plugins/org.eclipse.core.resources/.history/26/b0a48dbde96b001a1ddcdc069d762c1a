/*
 * SysDef.cpp
 *
 *  Created on: Mar 20, 2020
 *      Author: bill
 */

#include "../Include/SysDef.hpp"
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <string.h>
#include <iostream>

namespace SYS {

	_BYTE *Key_ = (_BYTE *)"a1234567890bcdef0132457689abcdef";
	_BYTE *InitialVector_ = (_BYTE *)"0123456789abcdef";

	const _UINT16 UDP_Port_RANGE_BEGIN = 10240;
	const _UINT16 UDP_Port_RANGE_END = UDP_Port_RANGE_BEGIN+10-1;



#pragma pack(push,1)
	struct PACKET {
		MASK_REQUEST Request_;
		_WORD Digest_;
	};
#pragma pack(pop)



	_WORD Fletcher16(_BUF &buf )	{
		_BYTE *Ptr = (_BYTE *)buf.Ptr();
		size_t Count= buf.DataLen();
		_WORD Sum1 = 0;
		_WORD Sum2 = 0;
		while ( Count ) {
			Sum1 = (Sum1 + (*Ptr)) & 0xff;
	      	Sum2 = (Sum2 + Sum1) & 0xff;
	      	--Count;
	      	++Ptr;
			}
		return (Sum2 << 8) | Sum1;
	}



	int _Encrypt(_BYTE *src_ptr, int src_len, _BYTE *output_ptr)
	{
	    EVP_CIPHER_CTX *CipherContext;

	    int Ret;
	    int Len;
	    int OutputLen;

	    CipherContext = EVP_CIPHER_CTX_new();

	    Ret=EVP_EncryptInit_ex(CipherContext, EVP_aes_256_cbc(), NULL, Key_, InitialVector_);
	    _XASSERT(Ret==1);

	    Ret=EVP_EncryptUpdate(CipherContext, output_ptr, &Len, src_ptr, src_len);
	    _XASSERT(Ret==1);

	    OutputLen = Len;

	    Ret=EVP_EncryptFinal_ex(CipherContext, output_ptr + Len, &Len);
	    _XASSERT(Ret==1);

	    OutputLen += Len;

	    EVP_CIPHER_CTX_free(CipherContext);

	    return OutputLen;
	}



	int _Decrypt(unsigned char *ciphertext, int ciphertext_len, unsigned char *src_ptr)
	{
	    EVP_CIPHER_CTX *CipherContext;

	    int Ret;
	    int Len;
	    int OutputLen;

	    CipherContext = EVP_CIPHER_CTX_new();

	    EVP_DecryptInit_ex(CipherContext, EVP_aes_256_cbc(), NULL, Key_, InitialVector_);
	    _XASSERT(Ret==1);

	    Ret=EVP_DecryptUpdate(CipherContext, src_ptr, &Len, ciphertext, ciphertext_len);
	    _XASSERT(Ret==1);

	    OutputLen = Len;
	    Ret=EVP_DecryptFinal_ex(CipherContext, src_ptr + Len, &Len);
	    _XASSERT(Ret==1);

	    OutputLen += Len;

	    EVP_CIPHER_CTX_free(CipherContext);

	    return OutputLen;
	}



	_OBJ<IMem> Encrypt(_OBJ<IMem> mem) {
		// Symmetric encryption, AES
	    _BYTE *EncryptBuf = new _BYTE[mem->DataLen() << 2];
	    int EncryptedDataLen = _Encrypt ((_BYTE *)mem->Ptr(), mem->DataLen(),EncryptBuf);
	    _OBJ<IMem> EncrypedData = _MEM::New(EncryptedDataLen,EncryptBuf);
	    delete EncryptBuf;
	    return EncrypedData;
	}



	_OBJ<IMem> Decrypt(_OBJ<IMem> mem) {
		_BYTE *DecrytBuf = new _BYTE[mem->DataLen() << 2];
	    _UINT DecrytedDataLen = _Decrypt((_BYTE *)mem->Ptr(), mem->DataLen(), DecrytBuf);
	    _XASSERT(DecrytBuf > 0);
	    _OBJ<IMem> Ret = _MEM::New(DecrytedDataLen,DecrytBuf);
	    delete DecrytBuf;
	    return Ret;
	}



	_OBJ<IMem> MASK_REQUEST::Serialize() {
		_OBJ<IMem> Data = _MEM::New(sizeof(PACKET));
		Data->DataLen(sizeof(PACKET));
		PACKET *pPacket = (PACKET *)Data->Ptr();
		pPacket->Request_ = *this;
		_BUF DigestBuf(this,sizeof(MASK_REQUEST));
		pPacket->Digest_ = Fletcher16(DigestBuf);

		_OBJ<IMem> EncryptedData = Encrypt(Data);
		return EncryptedData;
	}


	bool CheckDigest(PACKET *packet_ptr) {
		_BUF DigestBuf(&(packet_ptr->Request_),sizeof(MASK_REQUEST));
		return (packet_ptr->Digest_ == Fletcher16(DigestBuf) )?true:false;
	}



	bool CheckId(PACKET *packet_ptr) {
		// Check if ID is valid
		return true;
	}



	bool Deserialize(MASK_REQUEST *ret,_OBJ<IMem> mem) {
		_OBJ<IMem> DecrypedMem=Decrypt(mem);
		if ( !DecrypedMem ) {
			return false;
		}
		PACKET *pPacket = (PACKET *)DecrypedMem->Ptr();
		if ( !CheckDigest(pPacket) || !CheckId(pPacket)) {
			return false;
		}
		*ret = pPacket->Request_;
		return true;
	}



	void Initialize() {
		// NULL
	}



	void Shutdown() {
		// NULL
	}

};


