/*
 * PacketHandler.cpp
 *
 *  Created on: Mar 19, 2020
 *      Author: bill
 */

#include "./PacketHandler.hpp"
#include "./Def.hpp"
#include <cstring>
#include <Eden/Include/Gcd/Gcd.hpp>
#include <mutex>
#include <boost/filesystem.hpp>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>



namespace PACKET_HANDLER {

	const _UINT NUM_THREADS = 16;

	mutex Mutex_;
	GCD::TASK_QUEUE_ID TaskQueue_;
	vector<SYS::MASK_REQUEST> vectRequests_;



	void WriteToFile(SYS::MASK_REQUEST *data,_UINT num_entries) {
		_UUID Uuid;
		Uuid.Generate();
		_STRING strUuid = Uuid;
		_STRING TmpFilename = ".tmp." + strUuid;
		filesystem::path TmpFilePath(DEF::DIR_STOR / TmpFilename);
		int FileHandle = open(TmpFilePath.string().c_str(),O_CREAT | O_WRONLY);
		_XASSERT(FileHandle>0);
		_UINT DataLen = sizeof(SYS::MASK_REQUEST)*num_entries;
		ssize_t retWrite = write(FileHandle,data,DataLen);
		_XASSERT(retWrite==DataLen);
		close(FileHandle);
		filesystem::path FinalFilePath(DEF::DIR_STOR / strUuid);
		filesystem::rename(TmpFilePath,FinalFilePath);
		delete data;
	}



	void _Flush() {
		_UINT numEntries = vectRequests_.size();
		SYS::MASK_REQUEST *WriteData = new SYS::MASK_REQUEST[numEntries];
		GCD::Dispatch([=]{
			WriteToFile(WriteData,numEntries);
		},TaskQueue_);
	}



	void ThreadProc_RawPacket(sockaddr_in adr,_OBJ<IMem> data) {
		SYS::MASK_REQUEST Request;
		if ( SYS::Deserialize(&Request,data) ) {
			lock_guard<mutex> Lock(Mutex_);
			vectRequests_.push_back(Request);
			if (vectRequests_.size() >= DEF::FLUSH_BOUNDARY ) {
				_Flush();
			}
		}
	}



	void Process(sockaddr_in adr,_OBJ<IMem> data) {
		lock_guard<mutex> Lock(Mutex_);
		GCD::Dispatch([=]{
			ThreadProc_RawPacket(adr,data);
		},TaskQueue_);
	}



	void Initialize() {
		TaskQueue_ = GCD::CreateQueue(NUM_THREADS,GCD::SCHEDULE_POLICY::Idle,GCD::PRIORITY::Min);
	}



	void Shutdown() {
		// NULL
	}
};


