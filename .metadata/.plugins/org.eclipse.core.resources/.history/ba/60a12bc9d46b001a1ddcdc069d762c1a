/*
 * PacketHandler.cpp
 *
 *  Created on: Mar 19, 2020
 *      Author: bill
 */

#include "./PacketHandler.hpp"
#include "./Def.hpp"
#include <cstring>
#include <Eden/Include/Gcd/Gcd.hpp>
#include <mutex>
#include <boost/filesystem.hpp>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <iostream>
#include <set>



namespace PACKET_HANDLER {

	const _UINT NUM_THREADS = 16;
	_UINT Count_=0;

	mutex Mutex_;
	GCD::TASK_QUEUE_ID TaskQueue_;
	vector<SYS::MASK_REQUEST> vectRequests_;
	set<SYS::MASK_REQUEST> setRequest_;



	_UINT NumberOfProcessedRecords() {
		lock_guard<mutex> Lock(Mutex_);
		return Count_;
	}



	_UINT NumberOfValidRequests() {
		lock_guard<mutex> Lock(Mutex_);
		return setRequest_.size();
	}



	void WriteToFile(SYS::MASK_REQUEST *data,_UINT num_entries) {
		_UUID Uuid;
		Uuid.Generate();
		_STRING strUuid = Uuid;
		_STRING TmpFilename = ".tmp." + strUuid;
		filesystem::path TmpFilePath(DEF::DIR_STOR / TmpFilename);
		int FileHandle = open(TmpFilePath.string().c_str(),O_CREAT | O_WRONLY, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
		_XASSERT(FileHandle>0);
		_UINT DataLen = sizeof(SYS::MASK_REQUEST)*num_entries;
		ssize_t retWrite = write(FileHandle,data,DataLen);
		_XASSERT(retWrite==DataLen);
		close(FileHandle);
		filesystem::path FinalFilePath(DEF::DIR_STOR / strUuid);
		filesystem::rename(TmpFilePath,FinalFilePath);
		delete data;
	}



	void _Flush() {
		_UINT numEntries = vectRequests_.size();
		SYS::MASK_REQUEST *WriteData = new SYS::MASK_REQUEST[numEntries];
		SYS::MASK_REQUEST *Ptr = WriteData;
		for ( auto &Iter : vectRequests_ ) {
			*Ptr = Iter;
			++Ptr;
		}
		GCD::Dispatch([=]{
			WriteToFile(WriteData,numEntries);
		},TaskQueue_);
		vectRequests_.clear();
	}



	bool InBlackList(sockaddr_in adr) {
		return false;
	}



	void Process(sockaddr_in adr,_OBJ<IMem> data) {
		if ( InBlackList(adr) ) {
			return;
		}
		SYS::MASK_REQUEST Request;
		if ( SYS::Deserialize(&Request,data) ) {
			lock_guard<mutex> Lock(Mutex_);
			++Count_;
			if ( setRequest_.find(Request)  != setRequest_.end()) {
				vectRequests_.push_back(Request);
				if (vectRequests_.size() >= DEF::FLUSH_BOUNDARY ) {
					_Flush();
				}
			}
		}
	}



	void Initialize() {
		TaskQueue_ = GCD::CreateQueue(NUM_THREADS,GCD::SCHEDULE_POLICY::Other,GCD::PRIORITY::Normal);
	}



	void Shutdown() {
		// NULL
	}
};


