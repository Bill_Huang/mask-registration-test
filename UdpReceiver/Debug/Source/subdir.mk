################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../Source/Def.cpp \
../Source/Main.cpp \
../Source/PacketHandler.cpp 

OBJS += \
./Source/Def.o \
./Source/Main.o \
./Source/PacketHandler.o 

CPP_DEPS += \
./Source/Def.d \
./Source/Main.d \
./Source/PacketHandler.d 


# Each subdirectory must supply rules for building sources it contributes
Source/%.o: ../Source/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++0x -I../.. -I/opt/x86/usr/include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


