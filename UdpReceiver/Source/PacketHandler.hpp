/*
 * UpdReceiver.hpp
 *
 *  Created on: Mar 19, 2020
 *      Author: bill
 */

#pragma once
#ifndef SOURCE_PACKETHANDLER_HPP_
#define SOURCE_PACKETHANDLER_HPP_

#include <Sys/Include/SysDef.hpp>
#include <Eden/Include/Core/CoreMem.hpp>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>



namespace PACKET_HANDLER {
	void Process(int socket_fd,sockaddr_in,socklen_t,_OBJ<IMem>);
	_UINT NumberOfProcessedRecords();
	_UINT NumberOfValidRequests();
	void Initialize();
	void Shutdown();
};




#endif /* SOURCE_PACKETHANDLER_HPP_ */
