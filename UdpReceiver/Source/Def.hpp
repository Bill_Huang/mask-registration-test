/*
 * Def.hpp
 *
 *  Created on: Mar 19, 2020
 *      Author: bill
 */

#pragma once
#ifndef SOURCE_DEF_HPP_
#define SOURCE_DEF_HPP_

#include <Eden/Include/Core/CoreDef.hpp>
#include <boost/filesystem.hpp>



namespace DEF {

	extern const _UINT FLUSH_BOUNDARY;
	extern filesystem::path DIR_STOR;


};



#endif /* SOURCE_DEF_HPP_ */
